import networkx as nx
from networkx import astar_path_length, predecessor, dijkstra_path
import networkx.algorithms
from networkx.algorithms import dijkstra_path_length, bidirectional_dijkstra, single_source_dijkstra, \
    single_source_dijkstra_path, single_source_dijkstra_path_length, all_pairs_dijkstra_path, \
    all_pairs_dijkstra_path_length, dijkstra_predecessor_and_distance, bellman_ford, negative_edge_cycle, \
    goldberg_radzik, all_simple_paths

from networkx_viewer import Viewer
from collections import Counter
import itertools


### Filter example
# for n in G.nodes_iter():
#     G.node[n]['real'] = True

class myGraph(object):
    def __init__(self):

        self.G = nx.DiGraph()
        self.G.add_edge('BA', 'NY', weight=6)
        self.G.add_edge('BA', 'C', weight=5)
        self.G.add_edge('BA', 'CT', weight=4)
        self.G.add_edge('CT', 'NY', weight=8)
        self.G.add_edge('NY', 'L', weight=4)
        self.G.add_edge('L', 'CT', weight=6)
        self.G.add_edge('L', 'C', weight=3)
        self.G.add_edge('C', 'L', weight=3)
        self.G.add_edge('C', 'CT', weight=6)

        app = Viewer(self.G)

        print "Number of degrees of %s is %s" % ('BA', self.q0('BA'))
        print "Number of degrees of %s is %s" % ('L', self.q0('L'))

        print 'Route between %s and %s and %s is %s' % ('BA', 'NY', 'L', self.q1(['BA', 'NY', 'L']))
        print 'Route between %s and %s and %s is %s' % ('BA', 'C', 'L', self.q1(['BA', 'C', 'L']))
        print 'Route between %s and %s and %s and %s and %s is %s' % ('BA', 'CT', 'NY', 'L', 'CS', self.q1(['BA', 'CT', 'NY', 'L', 'CS']))
        print 'Route between %s and %s and %s is %s' % ('BA', 'CT', 'C', self.q1(['BA', 'CT', 'C']))

        print "Route between %s is" % self.q1(['NY', 'NY', 'NY', 'NY'])
        print "Route between 'BA', 'NY', 'L', 'C is %s" % self.q1(['BA', 'NY', 'L', 'C'])

        print "Shortest route between 'BA' and 'L' using the A* ('A-star') algorithm is %s" % self.q2('BA', 'L')
        print "Shortest route between 'NY' and 'NY' using the A* ('A-star') algorithm is %s" % self.q2('NY', 'NY')

        print "%s " % self.q3('L', 'L')
        print "%s " % self.q4('BA', 'L')
        print "Cost of route Liverpool to Liverpool via ['L', 'C', 'CT', 'NY', 'L'] is %s " % self.q5('L', 'L')


        return app.mainloop()

    def q0(self, *args):
        print ""
        print "Q0"
        return sorted(self.G.degree([args[0]]).values())

    def q1(self, mylist=[]):
        print ""
        print "Q1"
        accum_path = 0
        print 'Number of nodes is %s' % len(mylist)

        try:
            for i in xrange(len(mylist) - 1):
                accum_path += nx.dijkstra_path_length(self.G, mylist[i], mylist[i + 1])
            return accum_path
        except:
            return 0

    def q2(self, s, d):
        print ""
        print "Q2"
        return astar_path_length(self.G, s, d)

    def q3(self, s, d):
        # global paths
        print ""
        print "Q3"
        for path in nx.all_simple_paths(self.G, source=s, target=d):
            print 'Route between %s to %s : %s' % (s, d, path)
        paths = nx.all_simple_paths(self.G, source=s, target=d, cutoff=3)
        return 'All routes of %s to %s with maximum number of 3 stops: %s' % (s, d, list(paths))

    def q4(self, s, d):
        # global paths
        print ""
        print "Q4"
        paths = nx.all_simple_paths(self.G, source=s, target=d, cutoff=4)
        return 'All routes of %s to %s with up to 4 number of stops: %s' % (s, d, list(paths))

    def q5(self, s, d):
        # global paths
        print ""
        print "Q5"
        paths = nx.all_simple_paths(self.G, source=s, target=d)
        print 'All routes of %s to %s : %s' % (s, d, list(paths))

        print "Cost of route Liverpool to Liverpool via ['L', 'C', 'L']: %s" % (
            self.G.get_edge_data('L', 'C').get('weight') + self.G.get_edge_data('C', 'L').get('weight'))
        print "Cost of route Liverpool to Liverpool via ['L', 'C', 'CT', 'NY', 'L']: %s" % (
            self.G.get_edge_data('L', 'C').get('weight') + self.G.get_edge_data('C', 'CT').get(
                'weight') + self.G.get_edge_data('CT',
                                                 'NY').get(
                'weight') + self.G.get_edge_data('NY', 'L').get('weight'))
        print "Cost of route Liverpool to Liverpool via ['L', 'CT', 'NY', 'L']: %s" % (
            self.G.get_edge_data('L', 'CT').get('weight') + self.G.get_edge_data('CT', 'NY').get(
                'weight') + self.G.get_edge_data('NY',
                                                 'L').get(
                'weight'))

        return (s, d, list(paths))

if __name__ == '__main__':
    mygraph = myGraph()
