# README General Result #
```
Number of degrees in general: {'NY': 3, 'C': 4, 'CT': 4, 'BA': 3, 'L': 4}
Number of degrees 'BA': [3]
Number of degrees 'L': [4]

Q1
Buenos Aires to New York to Liverpool is: 10
Buenos Aires to Casablanca to Liverpool is: 8
Buenos Aires to Cape Town to New York to Liverpool to Casablanca is: 19
Route cannot be found

Q2
Shortest route between 'BA' and 'L' using the A* ('A-star') algorithm is: 8
Shortest route between 'NY' and 'NY' using the A* ('A-star') algorithm is: 0

Q3
Route between Liverpool to Liverpool : ['L', 'C', 'L']
Route between Liverpool to Liverpool : ['L', 'C', 'CT', 'NY', 'L']
Route between Liverpool to Liverpool : ['L', 'CT', 'NY', 'L']
All routes of Liverpool to Liverpool with maximum number of 3 stops: [['L', 'C', 'L'], ['L', 'CT', 'NY', 'L']]

Q4
All routes of Buenoes Aires to Liverpool with up to 4 number of stops: [['BA', 'NY', 'L'], ['BA', 'C', 'L'], ['BA', 'C', 'CT', 'NY', 'L'], ['BA', 'CT', 'NY', 'L']]

Q5
All routes of Liverpool to Liverpool less than 25 days: [['L', 'C', 'L'], ['L', 'C', 'CT', 'NY', 'L'], ['L', 'CT', 'NY', 'L']]
Cost of route Liverpool to Liverpool via ['L', 'C', 'L']: 6
Cost of route Liverpool to Liverpool via ['L', 'C', 'CT', 'NY', 'L']: 21
Cost of route Liverpool to Liverpool via ['L', 'CT', 'NY', 'L']: 18
```
This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact